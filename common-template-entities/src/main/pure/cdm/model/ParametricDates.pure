Class {meta::pure::profiles::doc.doc = 'Defines rules for the dates on which the price will be determined.'} cdm::model::ParametricDates
{
  {meta::pure::profiles::doc.doc = 'Denotes the enumerated values to specify the day type classification used in counting the number of days between two dates.'} dayType: cdm::model::DayTypeEnum[1];
  {meta::pure::profiles::doc.doc = 'Denotes the method by which the pricing days are distributed across the pricing period.'} dayDistribution: cdm::model::DayDistributionEnum[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the days of the week on which the price will be determined.'} dayOfWeek: cdm::model::DayOfWeekEnum[0..7];
  {meta::pure::profiles::doc.doc = 'Defines the occurrence of the dayOfWeek within the pricing period on which pricing will take place, e.g. the 3rd Friday within each Calculation Period. If omitted, every dayOfWeek will be a pricing day.'} dayFrequency: Float[0..1];
  {meta::pure::profiles::doc.doc = 'The pricing period per calculation period if the pricing days do not wholly fall within the respective calculation period.'} lag: cdm::model::Lag[0..1];
  {meta::pure::profiles::doc.doc = 'The enumerated values to specify the business centers.'} businessCalendar: cdm::model::BusinessCenterEnum[1];
}
