Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'Specifies the price, quantity, and optionally the observable for use in a trade or other purposes.'} cdm::model::PriceQuantity
[
  NonNegativeQuantity: if(
  $this.quantity.amount->isNotEmpty(),
  |$this.quantity.amount->toOne() > 0,
  |true
),
  condition1: if(
  $this.observable.rateOption->isNotEmpty() &&
    $this.price->isNotEmpty(),
  |$this.price.priceType->toOne()->in(
    [
      cdm::model::PriceTypeEnum.Spread,
      cdm::model::PriceTypeEnum.CapRate,
      cdm::model::PriceTypeEnum.FloorRate,
      cdm::model::PriceTypeEnum.MultiplierOfIndexValue
    ]
  ),
  |true
)
]
{
  <<cdm::model::metadata.location>> {meta::pure::profiles::doc.doc = 'Specifies a price to be used for trade amounts and other purposes.'} price: cdm::model::Price[*];
  <<cdm::model::metadata.location>> {meta::pure::profiles::doc.doc = 'Specifies a quantity to be associated with an event, for example a trade amount.'} quantity: cdm::model::Quantity[*];
  {meta::pure::profiles::doc.doc = 'Specifies the object to be observed for a price, it could be an asset or a reference.'} observable: cdm::model::Observable[0..1];
}
