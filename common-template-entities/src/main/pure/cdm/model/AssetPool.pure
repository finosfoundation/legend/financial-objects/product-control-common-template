Class {meta::pure::profiles::doc.doc = 'Characterizes the asset pool behind an asset backed bond.'} cdm::model::AssetPool
[
  AssetPool_effectiveDate: if(
  $this.version->isEmpty(),
  |$this.effectiveDate->isEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'The asset pool version.'} version: String[0..1];
  {meta::pure::profiles::doc.doc = 'Optionally it is possible to specify a version effective date when a version is supplied.'} effectiveDate: Date[0..1];
  {meta::pure::profiles::doc.doc = 'The part of the mortgage that is outstanding on trade inception, i.e. has not been repaid yet as principal. It is expressed as a multiplier factor to the mortgage: 1 means that the whole mortgage amount is outstanding, 0.8 means that 20% has been repaid.'} initialFactor: Float[1];
  {meta::pure::profiles::doc.doc = 'The part of the mortgage that is currently outstanding. It is expressed similarly to the initial factor, as factor multiplier to the mortgage. This term is formally defined as part of the \'ISDA Standard Terms Supplement for use with credit derivatives transactions on mortgage-backed security with pas-as-you-go or physical settlement\'.'} currentFactor: Float[0..1];
}
