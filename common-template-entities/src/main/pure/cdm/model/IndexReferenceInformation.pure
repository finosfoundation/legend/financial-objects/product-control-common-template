Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'A class defining a Credit Default Swap Index.'} cdm::model::IndexReferenceInformation
[
  IndexSeries: if(
  $this.indexSeries->isNotEmpty(),
  |$this.indexSeries >= 0,
  |true
),
  IndexAnnexVersion: if(
  $this.indexAnnexVersion->isNotEmpty(),
  |$this.indexAnnexVersion >= 0,
  |true
)
]
{
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'The name of the index expressed as a free format string with an associated scheme.'} indexName: String[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'A CDS index identifier (e.g. RED pair code).'} indexId: String[*];
  {meta::pure::profiles::doc.doc = 'A CDS index series identifier, e.g. 1, 2, 3 etc.'} indexSeries: Integer[0..1];
  {meta::pure::profiles::doc.doc = 'A CDS index series version identifier, e.g. 1, 2, 3 etc.'} indexAnnexVersion: Integer[0..1];
  {meta::pure::profiles::doc.doc = 'A CDS index series annex date.'} indexAnnexDate: Date[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'A CDS index series annex source.'} indexAnnexSource: cdm::model::IndexAnnexSourceEnum[0..1];
  {meta::pure::profiles::doc.doc = 'Excluded reference entity.'} excludedReferenceEntity: cdm::model::LegalEntity[*];
  {meta::pure::profiles::doc.doc = 'This element contains CDS tranche terms.'} tranche: cdm::model::Tranche[0..1];
  {meta::pure::profiles::doc.doc = 'Used to specify the Relevant Settled Entity Matrix when there are settled entities at the time of the trade.'} settledEntityMatrix: cdm::model::SettledEntityMatrix[0..1];
}
