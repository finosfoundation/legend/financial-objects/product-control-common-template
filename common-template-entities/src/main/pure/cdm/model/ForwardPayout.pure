Class {meta::pure::profiles::doc.doc = 'Represents a forward settling payout. The \'Underlier\' attribute captures the underlying payout, which is settled according to the \'SettlementTerms\' attribute. Both FX Spot and FX Forward should use this component.'} cdm::model::ForwardPayout
[
  SettlementDate: if(
  $this.underlier.underlyingProduct.foreignExchange->isNotEmpty(),
  |(($this.settlementTerms.valueDate->isNotEmpty() &&
    $this.underlier.underlyingProduct.foreignExchange.exchangedCurrency1.cashflowDate->isEmpty()) &&
    $this.underlier.underlyingProduct.foreignExchange.exchangedCurrency2.cashflowDate->isEmpty()) ||
    ((($this.settlementTerms.valueDate->isEmpty() &&
    $this.underlier.underlyingProduct.foreignExchange.exchangedCurrency1.cashflowDate->isNotEmpty()) &&
    $this.underlier.underlyingProduct.foreignExchange.exchangedCurrency2.cashflowDate->isNotEmpty()) &&
    ($this.underlier.underlyingProduct.foreignExchange.exchangedCurrency1.cashflowDate ==
    $this.underlier.underlyingProduct.foreignExchange.exchangedCurrency2.cashflowDate)),
  |true
),
  FxSettlement: if(
  $this.underlier.underlyingProduct.foreignExchange->isNotEmpty(),
  |$this.settlementTerms.cashSettlementTerms->isEmpty() &&
    $this.settlementTerms.physicalSettlementTerms->isEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'Underlying product that the forward is written on, which can be of any type: FX, a contractual product, a security, etc.'} underlier: cdm::model::Underlier[1];
  {meta::pure::profiles::doc.doc = 'Settlement terms for the underlier that include the settlement date, settlement method etc.'} settlementTerms: cdm::model::OptionSettlement[1];
}
